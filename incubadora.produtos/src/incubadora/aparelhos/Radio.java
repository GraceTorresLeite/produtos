package incubadora.aparelhos;

import incubadora.animais.Falador;

public class Radio implements Falador {
	@Override
	public void falar() {
		System.out.printf("O rádio toca música!%n");
	}

	@Override
	public String toString() {
		return String.format("Radio []");
	}
		
}
