package incubadora.produtos;

import java.util.Date;

public class ProdutoPerecivel extends Produto {

	private Date validade;

	public ProdutoPerecivel(String nome, double valor, Date validade) {
		super(nome, valor);
		this.validade = validade;
	}

	public Date getValidade() {
		return validade;
	}

	@Override
	public String toString() {
		return String.format("ProdutoPerecivel [validade=%s, toString()=%s]", validade, super.toString());
	}
	
	
	
}
