package incubadora.animais;

import incubadora.aparelhos.Radio;

public class Exemplos {
	public static void main(String[] args) {
		System.out.println("Animais");
		Falador r1 = new Radio();
		r1.falar();
		System.out.println(r1);
		print(r1);
		
		Gato g1 = new Gato("Mingau");
		g1.falar();
		System.out.println(g1);
		print(g1);
		
		Falador c1 = new Cao("Floquinho");
		c1.falar();
		System.out.println(c1);
		print(g1);

		Falador p1 = new Cao("Floquinho");
		p1.falar();
		System.out.println(p1);
		print(g1);
		
		System.out.println();
		
		Falador[] falantes = { r1, g1, c1, p1, new Cao("Bidu") };

		for (Falador falador : falantes) {
			falador.falar();
			System.out.println(falador);
			print(falador);
		}
	}

	private static void print(Object object) {
		System.out.printf("Print Object %s.%n", object);
	}
	
	private static void print(Gato gato) {
		System.out.printf("Print Gato %s.%n", gato);
	}
}
