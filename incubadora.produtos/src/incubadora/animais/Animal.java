package incubadora.animais;

public abstract class Animal implements Falador {
	protected String nome;

	public Animal(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public abstract void falar();

}
