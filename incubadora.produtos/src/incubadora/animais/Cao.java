package incubadora.animais;

public class Cao extends Animal {

	public Cao(String nome) {
		super(nome);
	}

	@Override
	public void falar() {
		System.out.printf("O cão %s falou Au-au!%n", nome);
	}

	@Override
	public String toString() {
		return String.format("Cao [nome=%s]", nome);
	}

}
