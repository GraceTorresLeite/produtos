package incubadora.animais;

public class Pato extends Animal {

	public Pato(String nome) {
		super(nome);
	}

	@Override
	public void falar() {
		System.out.printf("O pato %s falou Quack!%n", nome);
	}

	@Override
	public String toString() {
		return String.format("Pato [nome=%s]", nome);
	}

}
